from codecs import getencoder
from distutils.log import fatal
from tkinter import *
from tkinter import ttk
import sqlite3
from turtle import bgcolor

root = Tk()
# root.geometry("400x400")
root.title("Kostappen")

# Create info window function


def new_window():
    root1 = Tk()
    root1.title("Info")
    root1.geometry("400x200")

    info_label = Label(
        root1, text="Så här använder du kostappen \n", font=14)
    info_label.grid(row=1, column=1,)
    info_text = Label(
        root1, text="Kostappen är ett verktyg som hjälper dig att att hålla koll på vilken\n \
         mängd livsmedel du behöver per dag för att nå dina mål.\n Varje enhet av livsmedel motsvarar ca 100 kcal.\n \
             Fyll i dina uppgifter och klicka på result för att få ditt dagsbehov. \n ")
    info_text.grid(row=3, column=1)

# Create a close button
    def close():
        root1.destroy()
    info_button = Button(root1, text="Close", command=close)
    info_button.grid(row=15, column=1, columnspan=2,
                     pady=10, padx=10, ipadx=100)

    root1.mainloop()


def result():
    root1 = Tk()
    root1.title("Result")
    root1.geometry("800x400")

    gender_get = gender.get()
    activity1 = activity.get()
    goal1 = goal.get()

    if gender_get == "Male":
        gender_value = 5
    if gender_get == "Female":
        gender_value = -161

    if activity1 == "Low activity":
        activity_value = 400
    if activity1 == "Moderate activity":
        activity_value = 700
    if activity1 == "High activity":
        activity_value = 1000

    if goal1 == "Loose weight":
        goal_value = -500
    if goal1 == "Keep weight":
        goal_value = 0
    if goal1 == "Gain weight":
        goal_value = 500

    age1 = float(age.get())
    weight1 = float(weight.get())
    height1 = float(height.get())
    res = (weight1*10)+(height1*6.25)-(age1*5) + \
        gender_value+int(activity_value)+int(goal_value)
    # print(res)

    round_res = round(res/100)
    # print(round_res)

    protein = round(round_res*0.3)
    fat = round(round_res*0.3)
    carbs = round(round_res*0.4)
    if protein+fat+carbs > round_res:
        carbs -= 1
    if protein+fat+carbs < round_res:
        protein += 1
    # print(protein)
    # print(fat)
    # print(carbs)

    info_label = Label(
        root1, text="Ditt dagliga rekommenderade kaloriintag: " + str(res) + " kcal", font=10)
    info_label.grid(row=1, column=1,)

    info_label1 = Label(root1, text="Detta motsvarar " +
                        str(round_res) + " enheter", font=10)
    info_label1.grid(row=2, column=1,)
    # räkna på E% 30%P 30%F 40%K
    info_label2 = Label(root1, text="Förelningen per dag blir: \n " +
                        str(protein) + " enheter protein\n " + str(fat) + " enheter fett\n"+str(carbs)+" enheter kolhydrater\n", font=10)
    info_label2.grid(row=3, column=1)

    database = sqlite3.connect("database.db")
    curs = database.cursor()

    curs.execute("SELECT * FROM protein")
    protein = curs.fetchall()
    curs.execute("select * from carbs")
    carbs = curs.fetchall()
    curs.execute("select * from fat")
    fat = curs.fetchall()
    protein_label = Label(root1, text="Protein: " + str(protein), font=8)
    protein_label.grid(row=5, column=1)
    fat_label = Label(root1, text="Fett: " + str(fat), font=8)
    fat_label.grid(row=6, column=1)
    carbs_label = Label(root1, text="Kolhydrater: " + str(carbs), font=8)
    carbs_label.grid(row=7, column=1)

    def close():
        root1.destroy()
        info_button = Button(root1, text="Close", command=close)
        info_button.grid(row=15, column=1, columnspan=2,
                         pady=10, padx=10, ipadx=100)

    root1.mainloop()


def delete():
    age.delete(0, END)
    weight.delete(0, END)
    height.delete(0, END)
    gender.delete(0, END)
    activity.delete(0, END)
    goal.delete(0, END)


def open_db():
    database = sqlite3.connect("database.db")
    curs = database.cursor()

    # query the database
    curs.execute("SELECT * FROM protein")
    protein = curs.fetchall()
    curs.execute("select * from carbs")
    carbs = curs.fetchall()
    curs.execute("select * from fat")
    fat = curs.fetchall()
    print(protein)
    print(carbs)
    print(fat)
    # show_protein = protein.get()
    # show_carbs = carbs.get()
    # show_fat = fat.get()
    protein_label = Label(root, text=protein)
    protein_label.grid(row=20, column=1)
    carbs_label = Label(root, text=carbs)
    carbs_label.grid(row=22, column=1)
    fat_label = Label(root, text=fat)
    fat_label.grid(row=24, column=1)
    database.commit()
    database.close()
    root.mainloop()


# create textboxes/comboxes
age = Entry(root, width=4)
age.grid(row=0, column=1,)
weight = Entry(root, width=4)
weight.grid(row=1, column=1,)
height = Entry(root, width=4)
height.grid(row=2, column=1,)
gender = ttk.Combobox(root, width=6, values=["Male", "Female"])
# .set ger ett default värde
gender.set("Male")
gender.grid(column=1, row=3)
activity = ttk.Combobox(root, width=17, values=[
                        "Low activity", "Moderate activity", "High activity"])
activity.set("Moderate activity")
activity.grid(row=4, column=1,)
goal = ttk.Combobox(root, width=13, values=[
                    "Loose weight", "Keep weight", "Gain weight"])
goal.set("Keep weight")
goal.grid(row=5, column=1,)

# create text box labels
age_label = Label(root, text="Age")
age_label.grid(row=0, column=0)
weight_label = Label(root, text="Weight (kilograms)")
weight_label.grid(row=1, column=0)
height_label = Label(root, text="Height (centimeters)")
height_label.grid(row=2, column=0)
gender_label = Label(root, text="Gender")
gender_label.grid(row=3, column=0)
activity_label = Label(root, text="Activity level")
activity_label.grid(row=4, column=0)
goal_label = Label(root, text="Goal")
goal_label.grid(row=5, column=0)

# create a result button - def result()
result_button = Button(root, text="Result", command=result)
result_button.grid(row=7, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

# Info button - def new_window()
info_button = Button(root, text="Info", command=new_window,
                     bg="blue", activebackground="red")
info_button.grid(row=8, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

# create a delete button - def delete()
delete_button = Button(root, text="Delete", command=delete)
delete_button.grid(row=9, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

# create test button from database.db
# db_button = Button(root, text="DB", command=open_db)
# db_button.grid(row=10, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

# database.commit()
# database.close()

root.mainloop()
