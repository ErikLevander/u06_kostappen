BEGIN;

INSERT INTO protein (name, amount) VALUES 
    ('Kyckling', '100g'), 
    ('Tonfisk', '100g'),
    ('Köttfärs 5%', '80g'),
    ('Lax', '55g'),
    ('Quorn', '100g');

INSERT INTO fat (name, amount) VALUES 
    ('Avokado', 'En halv'), 
    ('Nötter', '15g'),
    ('Olivolja', '1 msk');

INSERT INTO carbs (name, amount) VALUES 
    ('Potatis', '120g'), 
    ('Ris, tillagat', '90g'),
    ('Havregryn', '30g'),
    ('Banan', '1 normalstor');
COMMIT;
