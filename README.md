
# Kostappen

### Om kostappen
Kostappen är ett hjälpmedel för dig som vill öka, minska eller behålla din vikt genom att ge förslag på livsmedel och hur stor mängd som motsvarar ditt dagliga intag.
Varje livsmedel är uppdelat i enheter fördelat på protein, kolhydrater och fett. Varje enhet motsvarar 100kcal.

### Hur kostappen fungerar

![gui](https://gitlab.com/ErikLevander/u06_kostappen/-/raw/main/gui.png)
Fyll i dina uppgifter och klicka på result:
![result](https://gitlab.com/ErikLevander/u06_kostappen/-/raw/main/result.png)
